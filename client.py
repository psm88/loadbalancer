import random

from entities.book import Book
from repositories.abstractRepository import Repository


def test_add_book(book_repository, i, id):
        book = Book()
        book.title = "title" + str(i) + "id" + str(id)
        book.author = "author" + str(i) + "id" + str(id)
        book_repository.add(book)


def test_update_book(book_repository, currently_inserted_books, currently_deleted_books, i, id):
    if currently_inserted_books > currently_deleted_books:
        r = random.randint(currently_deleted_books + 1, currently_inserted_books)
        book_test = Book()
        book = book_repository.find_by_id(Book, r)
        if(book == None):
            return
        attr = "title"
        value = "title" + str(i) + "id" + str(id) + "u"
        book_repository.update(Book, book, attr, value)


def test_delete_book(book_repository, currently_inserted_books, currently_deleted_books):
    if currently_inserted_books > currently_deleted_books:
        book = book_repository.find_by_id(Book, currently_deleted_books+1)
        if(book == None):
            return
        book_repository.remove(Book, book)


def test_select_all_book(book_repository, currently_inserted_books, currently_deleted_books):
    if currently_inserted_books > currently_deleted_books:
        book_repository.find_all(Book)


def test_select_book(book_repository, currently_inserted_books, currently_deleted_books):
    if currently_inserted_books > currently_deleted_books:
        r = random.randint(currently_deleted_books+1, currently_inserted_books)
        book_repository.find_by_id(Book, r)


def test_sqlalchemy_orm(entity_manager, id, n=100000, insert_percentage=20, update_percentage=5, delete_percentage=5,
                        select_percentage=60, select_all_percentage=10):

    book_repository = Repository(entity_manager)

    stop = insert_percentage + update_percentage + delete_percentage + select_percentage + select_all_percentage

    currently_inserted_books = 0
    currently_deleted_books = 0
    for i in range(n):
        r = random.randrange(stop)
        if r < insert_percentage:
            test_add_book(book_repository, i+1, id)
            currently_inserted_books += 1
        elif r < insert_percentage + update_percentage:
            test_update_book(book_repository, currently_inserted_books, currently_deleted_books, i+1, id)
        elif r < insert_percentage + update_percentage + delete_percentage:
            test_delete_book(book_repository, currently_inserted_books, currently_deleted_books)
            currently_deleted_books += 1
        elif r < insert_percentage + update_percentage + delete_percentage + select_percentage:
            test_select_book(book_repository, currently_inserted_books, currently_deleted_books)
        else:
           test_select_all_book(book_repository, currently_inserted_books, currently_deleted_books)
    print('\n\nFINISHED')