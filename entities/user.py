from sqlalchemy import Column, Integer, String

from config.entityManager import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)

    def clone(self, src):
        self.name = src.name

    def __repr__(self):
        return "<User(name='{}')>".format(self.name)
