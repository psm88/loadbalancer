from sqlalchemy.dialects import postgresql

from config.entityManager import EntityManager


class Repository:

    em: EntityManager

    def __init__(self, em: EntityManager) -> None:
        self.em = em

    def find_all(self, classname):
        with self.em.session_scope() as s:
            data = s.query(classname).all()
        if (len(data) == 0):
            print("No data to show.")
        else:
            print("Data to show:")
            for obj in data:
                print(obj)

    def find_by_id(self, classname, obj_id):
        with self.em.session_scope() as s:
            val_filter = s.query(classname).filter(classname.id == obj_id)
            self.print_query(val_filter)
            first = val_filter.first()
            if (first is not None):
                print("Object with given id:")
                print(first)
                return first
            else:
                print("No object with such id.")

    def find_by_attr(self, classname, attr, value):
        with self.em.session_scope() as s:
            objs = s.query(classname).filter(attr == value).all()
            if (len(objs) == 0):
                print("No object with given attribute.")
            else:
                print("Following objects found:")
                for obj in objs:
                    print(obj)

    def add(self, obj):
        with self.em.session_scope() as s:
            s.add(obj)
            print(obj)
            print("Given object added.")

    def update(self, classname, obj, attr, value):
        with self.em.session_scope() as s:
            s.query(classname).filter(classname.id == obj.id).update({attr: value})
            obj.attr = value
            print("Object updated.")

    def update_by_id(self, classname, objid, attr, value):
        new_obj = self.find_by_id(classname, objid)
        with self.em.session_scope() as s:
            s.query(classname).filter(classname.id == new_obj.id).update({attr: value})
            new_obj.attr = value
            print("Object updated.")

    def remove(self, classname, obj):
        with self.em.session_scope() as s:
            s.query(classname).filter(classname.id == obj.id).delete()
            print("Given object removed.")

    def print_query(self, query):
        print('\n', str(query.statement.compile(dialect=postgresql.dialect())), '\n')
