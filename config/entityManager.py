import re
import threading
from contextlib import contextmanager

import time

import sys
from sqlalchemy import create_engine, event, exc
from sqlalchemy.engine import Engine
import time
from sqlalchemy import create_engine, event, exc
from sqlalchemy.exc import DisconnectionError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from config.config import DATABASE_URI_ARRAY
from config.unitOfWork import UnitOfWork

Base = declarative_base()


class EntityManager:
    index = 0
    sessions = []
    engines = []
    # active_session = 0
    states_are_current = []
    units_of_work = []

    def __init__(self):
        for uri in DATABASE_URI_ARRAY:
            print(uri)
            self.create_context(uri)
            self.states_are_current.append(True)
            self.units_of_work.append(None)

        # self.active_session = self.sessions[self.index]()
        self.drop_all()
        self.create_all()
        # event.listen(Engine, 'dbapi_error', self.on_error)
        self.states_are_current.append(True)
        self.units_of_work.append(None)

       # self.drop_all()
       # self.create_all()
        event.listen(Engine, 'before_cursor_execute', self.before)

    def before(self, conn, cursor, statement, parameters, context, executemany):

        raw_statement = statement
        print("baza: ", conn.engine.url)
        print("statement: %s" % statement)
        if parameters is not None and parameters:
            statement = re.sub(r'%\(([^\)]*)\)s', r':\1', statement)
        # print("statement: %s" % statement)
        if re.search('.*(CREATE|INSERT|UPDATE|DELETE|DROP).*', statement):
            # print('change')
            url = conn.engine.url
            # todo enable dynamic insert
            if self.engines[0].url.database != url.database:
                try:
                    return
                except:
                    i = self.engines.index(conn.engine)
                    if self.states_are_current[i]:
                        self.states_are_current[i] = False
                        self.units_of_work[i] = UnitOfWork()
                        ThreadToReconnect(self, i).start()
                    self.units_of_work[i].addQuery(statement, parameters)
                    print("Database server went away")

            print('event before cursor: ', url)

            for i, engine in enumerate(self.engines):
                if engine.url != url:
                    print('execute replicatation for {}'.format(engine.url))
                    s = self.sessions[i]()
                    try:
                        s.execute(statement, parameters)
                        s.commit()
                        s.close()
                    except:
                        if self.states_are_current[i]:
                            self.states_are_current[i] = False
                            self.units_of_work[i] = UnitOfWork()
                            ThreadToReconnect(self, i).start()
                        self.units_of_work[i].addQuery(statement, parameters)
                        print("Database server went away")
        # elif re.search('.*(SELECT).*', statement) and not(re.search('.*(SELECT PG_CLASS).*', statement)):
        #     self.before_select(conn, raw_statement, parameters)
        else:
            try:
                print("here")
                while True in self.states_are_current:
                    session = self.load_balance()
                    if session is None:
                        i = self.engines.index(conn.engine)
                        if self.states_are_current[i]:
                            self.states_are_current[i] = False
                            self.units_of_work[i] = UnitOfWork()
                            ThreadToReconnect(self, i).start()
                    else:
                        break;


            except:
                i = self.engines.index(conn.engine)
                if self.states_are_current[i]:
                    self.states_are_current[i] = False
                    self.units_of_work[i] = UnitOfWork()
                    ThreadToReconnect(self, i).start()
                self.units_of_work[i].addQuery(statement, parameters)
                print("Database server went away")


    def create_context(self, uri):
        engine = create_engine(uri)
        session = sessionmaker(bind=engine, expire_on_commit=False)
        self.engines.append(engine)
        self.sessions.append(session)

    def drop_all(self):
        for engine in self.engines:
            Base.metadata.drop_all(engine)

    def create_all(self):
        for engine in self.engines:
            Base.metadata.create_all(engine)

    def load_balance(self):
        number_of_disconnected_bases = 0
        while not self.states_are_current[self.index]:
            self.index = self.index + 1
            number_of_disconnected_bases = number_of_disconnected_bases + 1
            if number_of_disconnected_bases > len(self.sessions):
                print("All bases are disconnected. Do something with this!")
                return None

        active_session = self.sessions[self.index]()
        print('current session : {}'.format(active_session.bind.engine.url))
        self.index = self.index + 1
        if self.index >= len(self.sessions):
            self.index = 0

        return active_session

    def on_error(self, conn, cursor, statement, parameters, context, exception):
        if "connection" in str(exception):
            print("statement: %s" % statement)
            if parameters is not None and parameters:
                statement = re.sub(r'%\(([^\)]*)\)s', r':\1', statement)
            print("statement: %s" % statement)

            i = self.engines.index(conn.engine)
            if self.states_are_current[i]:
                self.states_are_current[i] = False
                self.units_of_work[i] = UnitOfWork()
                ThreadToReconnect(self, i).start()
            if re.search('.*(CREATE|INSERT|UPDATE|DELETE|DROP).*', statement):
                self.units_of_work[i].addQuery(statement, parameters)
            print("Database server went away")



    @contextmanager
    def session_scope(self):
        session = self.sessions[0]()
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            # raise
        finally:
            session.close()

    @contextmanager
    def dynamic_session_scope(self):
        session = self.load_balance()
        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            # raise
        finally:
            session.close()

    def choose_base(self, conn, statement, parameters):

        # conn.execute(statement, parameters)
        self.load_balance()

    def try_reconnect(self, index):
        reconnected = False
        while not reconnected:
            time.sleep(5)
            try:
                print("im trying to reconnect")
                self.engines[index] = create_engine(DATABASE_URI_ARRAY[index])
                self.sessions[index] = sessionmaker(bind=self.engines[index])

                if self.units_of_work[index].updateBase(self.sessions[index]) == 0:
                    raise exc.DBAPIError
                self.states_are_current[index] = True
                print("reconnected ", index)
                reconnected = True

            except Exception as e:
                print()
                # if e.connection_invalidated:
                #     print("not reconnected")



class ThreadToReconnect(threading.Thread):
    def __init__(self, em, index):
        threading.Thread.__init__(self)
        self.em = em
        self.index = index

    def run(self):
        self.em.try_reconnect(self.index)








