from sqlalchemy import exc


class UnitOfWork:
    def __init__(self):
        self.list_of_queries = []

    def addQuery(self, statement, parameters):
        self.list_of_queries.append((statement, parameters))

    def updateBase(self, session):
        try:
            s = session()
            for query in self.list_of_queries:
                s.execute(query[0], query[1])
            s.commit()
            s.close()
            print(self.list_of_queries)
        except exc.OperationalError:
            return 0

